import * as THREE from 'three'
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls'
// import {PointerLockControls} from 'three/examples/jsm/controls/PointerLockControls'
// import {DragControls} from 'three/examples/jsm/controls/DragControls'

// import {FlyControls} from 'three/examples/jsm/controls/FlyControls'
import {FBXLoader} from 'three/examples/jsm/loaders/FBXLoader'
import Stats from 'three/examples/jsm/libs/stats.module'
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader'
import * as CANNON from 'cannon'
import {CSS3DObject, CSS3DRenderer} from 'three/examples/jsm/renderers/CSS3DRenderer'

const camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
)

camera.position.z = 1
camera.position.x = -1
camera.position.y = 0.5
// camera.position.y = 1

// const cssRenderer = new CSS3DRenderer()
// cssRenderer.setSize(window.innerWidth, window.innerHeight)

const renderer = new THREE.WebGLRenderer()
renderer.shadowMap.enabled = true
renderer.setSize(window.innerWidth, window.innerHeight)
renderer.setClearColor(0xffffff)

document.body.appendChild(renderer.domElement)

const scene = new THREE.Scene()

const light = new THREE.SpotLight(0xffffff)
light.position.set(1, 10, 10)
scene.add(light)

const light2 = new THREE.PointLight(0xffffff, 2)
light2.position.set(10, 10, -10)
scene.add(light2)

// const controls = new PointerLockControls(camera, renderer.domElement)
// controls.addEventListener('lock', () => ())
// controls.addEventListener('unlock', () => ())

// const controls = new FlyControls(camera, renderer.domElement)
// controls.dragToLook = true;
// controls.autoForward = false;
// controls.movementSpeed = 10;
// controls.rollSpeed = 1;

const controls = new OrbitControls(camera, renderer.domElement)
controls.enableDamping = true
controls.target.set(0, 0, 0)

// const floorGeometry = new THREE.PlaneGeometry(10, 10)
// const floorMaterial = new THREE.MeshPhongMaterial({
//     color: 0x403F3C,
// })
// const floor = new THREE.Mesh(floorGeometry, floorMaterial)
// floor.rotateX(-Math.PI / 2)
// floor.receiveShadow = true
// scene.add(floor)

// #GLTF on floor
const loader = new GLTFLoader()
loader.load('models/footballfield.glb', 
    function(gltf) {
        gltf.scene.position.y = 0.01
        gltf.scene.position.z = 1.1
        gltf.scene.position.x = 0.44
        gltf.scene.name = 'football-field'
        scene.add(gltf.scene)
    }
)
const fbxload = new FBXLoader()
fbxload.load('models/ground.fbx', 
    function(object) {
        object.position.y = 0.01
        object.position.z = 1.1
        object.position.x = 0.4
        object.scale.set(.01, .01, .01)
        object.name = 'earth'
        scene.add(object)
    }
)
fbxload.load('models/a_hospital.fbx', 
    function(object) {
        object.position.y = 0.01
        object.position.z = 0.96
        object.position.x = 0.54
        object.scale.set(.01, .01, .01)
        object.name = 'clinic'
        scene.add(object)
    }
)
fbxload.load('models/a_office.fbx',
    function(object){
        object.position.y = 0.01
        object.position.z = 1.31
        object.position.x = 0.37
        object.scale.set(.01, .01, .01)
        object.name = 'office'
        scene.add(object)
    }
)

// const boxGeometry = new THREE.BoxGeometry()
// const sphereGeometry = new THREE.SphereGeometry(0.2)
// const planeGeometry = new THREE.PlaneGeometry(3.6, 1.8)
// const boxMaterial = new THREE.MeshPhongMaterial({
//     color: 0x5b5066,
//     reflectivity: 0.3,
//     refractionRatio: 1
// })
// const sphereMaterial = new THREE.MeshPhongMaterial({
//     color: 0xdb4437,
//     reflectivity: 0.5,
//     transparent: true,
// })
// const material = new THREE.MeshPhongMaterial()
// const texture = new THREE.TextureLoader().load('img/maps-of-middleeast.png')
// material.map = texture
// const bumpTexture = new THREE.TextureLoader().load('img/maps-of-middleeast.png')
// material.bumpMap = bumpTexture
// material.bumpScale = 0.5

// const cube = new THREE.Mesh(boxGeometry, boxMaterial)
// cube.position.z = -0.51
// cube.position.y = 1
// cube.scale.x = 8
// cube.scale.y = 2
// cube.castShadow = true
// scene.add(cube)

// const sphere = new THREE.Mesh(sphereGeometry, sphereMaterial)
// sphere.position.x = -4
// sphere.position.y = 0.2
// sphere.position.z = 0
// sphere.castShadow = true
// sphere.name = 'Bolabola'
// scene.add(sphere)

// const sphereControls = new DragControls([sphere], camera, renderer.domElement)
// sphereControls.addEventListener("hoveron", function (event) {
//     event.object.material.opacity = 0.33
// })
// sphereControls.addEventListener("hoveroff", function (event) {
//     event.object.material.opacity = 1
// })
// sphereControls.addEventListener("dragstart", function(event){
//     halfMenuPanel.style.display = 'block'
//     controls.unlock()
// })
// sphereControls.addEventListener("dragend", function (event) {
//     sphere
// })

// const plane = new THREE.Mesh(planeGeometry, material)
// plane.position.y = 1
// scene.add(plane)

// const onKeyDown = function(event: KeyboardEvent) {
//     console.log(event.code)
//     switch (event.code){
//         case 'KeyW':
//             controls.moveForward(0.05)
//             break
//         case 'KeyS':
//             controls.moveForward(-0.05)
//             break
//         case 'KeyA':
//             controls.moveRight(-0.05)
//             break
//         case 'KeyD':
//             controls.moveRight(0.05)
//             break
//         case 'Escape':
//             halfMenuPanel.style.display = 'none'
//             menuPanel.style.display = 'block'
//     }
// }
// document.addEventListener('keydown', onKeyDown, false)

var isPlay = true

// const menuPanel = document.getElementById('menuPanel') as HTMLDivElement
const startButton = document.getElementById('startButton') as HTMLInputElement
const halfMenuPanel = document.getElementById('propertiesPanel') as HTMLDivElement
const titleExplain = document.getElementById('explanation') as HTMLHeadingElement
halfMenuPanel.style.display = 'none'
startButton.addEventListener('click', function () {
    halfMenuPanel.style.display = 'none'
    setTimeout(function(){ controls.enabled = true }, 1000);
}, false)

var raycaster = new THREE.Raycaster()
let mouse = {x:0,y:0}
window.addEventListener('mousedown', (e) =>{
    mouse.x = (e.clientX/window.innerWidth)*2-1
    mouse.y = (e.clientY/window.innerHeight)*-2+1
    // console.log(mouse);

    raycaster.setFromCamera(mouse, camera);
    let items = raycaster.intersectObjects(scene.children);
    // console.log(items[0].object.parent?.name)
    switch (items[0].object.parent?.name) {
        case "office":
            controls.enabled = false
            halfMenuPanel.style.display = 'block'
            titleExplain.innerHTML = "Tier 1 Office Building"
            break;
        case "clinic":
            controls.enabled = false
            halfMenuPanel.style.display = 'block'
            titleExplain.innerHTML = "Public Services: Clinic"
            break;
        case "Stadium_level_3":
            controls.enabled = false
            halfMenuPanel.style.display = 'block'
            titleExplain.innerHTML = "Leisure Services: Stadium"
            break;
        default:
            break;
    }
    // items.forEach((i) => {
        // console.log(i)
        // if (i.object.name != "") {
        //     if (i.object.parent?.name == "office") {
        //         alert("messej")
        //     }
        // }
    // })
})

window.addEventListener('resize', onWindowResize, false)
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
    render()
}

// const stats = Stats()
// document.body.appendChild(stats.dom)


function animate() {
    requestAnimationFrame(animate)

    // cube.rotation.x += 0.01
    // cube.rotation.y += 0.01

    render()
    // stats.update()
}

function render() {
    renderer.render(scene, camera)
}

animate()
// render()